# Unified in-vehicle SW orchestration - “Master Controller”

[Wiki documentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/vehicle-sw-orchestration-sdv-topic/-/wikis/home) 

# Related SDV projects:
-  [Eclipse Leda](https://projects.eclipse.org/projects/automotive.leda) 
-  [Eclipse Velocitas](https://projects.eclipse.org/projects/automotive.velocitas) 
-  [Eclipse Chariott](https://projects.eclipse.org/projects/automotive.chariott) 
-  [Eclipse Kuksa](https://projects.eclipse.org/projects/automotive.kuksa)

# Use cases:
-   “v1” - orchestrate workloads in SDV space (see diagram above, between the lines)
    -  Deploy a multi-container/artifact workload into SDV space
-  “v2” - orchestrate workloads across in-vehicle boundaries
    -  Deploy a multi-artifact workload into vehicle, across technology boundaries (e.g. include an Adaptive AUTOSAR UCM and an embedded controller in orchestration)

# Cross Initiatives Collaboration 
- Rancher/k3s
- COVESA (EPAM AOS) - in particular, the Vehicle Signal Specification might be a candidate for vehicle abstraction.
- OpenADKit
- For a “v2” (where a control plane might be extended towards other in-vehicle domains: 
    -  AUTOSAR (UCM)
    -  Things like Uptane
- SOAFEE
    -  Promote overall vision (as laid out in this presentation)
    -  Push subgroups to address some implementation aspects
    -  Mixed Criticality portability
    -  Secure standard device assignment for type 1 hypervisors (via Francois)
    -  Freedom of interference from secure firmware
- Linaro
    -  Trusted sensors (sensor signing component from different entity than the sensor business logic) => link to confidential compute




