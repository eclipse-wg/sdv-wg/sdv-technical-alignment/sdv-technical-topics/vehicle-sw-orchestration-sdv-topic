## Software Orchestration & Resource Scheduling Workstream

## Description
Welcome to the Software Orchestration & Resource Scheduling Workstream repo. We aim to define one concrete software architecture that can be implemented on an in-vehicle computer which supports the following use cases:

### UC1 - Deploy and Manage non-safety critical application

Automate deployment of a (multi-container/-artifact) application into the in-vehicle computer, schedule, manage and run it.

### UC2 - Deploy and Manage mixed-criticality application components to multiple targets

Automate deployment of an application that consists of artifacts of mixed criticality (QM & ASIL-B...) that need to be installed, managed and run in ( a containerization platform ) dedicated platforms/controllers.

### UC3 - Dynamically deploy application components to multiple targets (wishlist)

Dynamic deployment of application containers into the containerization platform across the different controllers/partitions based on resource availability.

### UC4 Download and Install application components to multiple targets (wishlist)

Download and manage the installation of the application components (inc. configuration and dependencies) across the multiple targets within the vehicle E/E platform - the automotive application components are generally distributed over multiple targets within the vehicle. For example, an embedded control unit (ECU) that provides safety-critical functionality needs to have its firmware updated and/or an AUTOSAR application needs to be deployed using AUTOSAR Update and Configuration Management (UCM).

## Authors and acknowledgment
**Facilitator**: Filipe Prezado (Microsoft), Kai Hudalla (Bosch.IO GmbH)

**Core participants**:

- Filipe Prezado, Sergey Markelov, Liviu Tiganus (Microsoft)
- Sven Erik Jeroschewski (Bosch.IO GmbH)
- Kai Hudalla (Bosch.IO GmbH)
- Badii Ennouri (badii.ennouri@gmail.com)
- Aymeric Rateau (Toyota)
- Thilo Schmitt (Mercedes-Benz Tech Innovation)
- Holger Dormann (Elektrobit)

## Contributing
The workstream meets every other week, on Wednesdays at 4pm CET. Please reach out to any of the participants if you would like to be added to the calendar invite.  

WIKI can be assessed [here](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/vehicle-sw-orchestration-sdv-topic/-/wikis/home)

Meetings notes can be assessed [latest here](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/vehicle-sw-orchestration-sdv-topic/-/wikis/Meeting-Minutes/2023-07-26-Recurring-Meeting)


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
